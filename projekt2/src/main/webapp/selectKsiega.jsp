<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h4>Wyszukiwanie wpisu z Czerwonej Księgi w bazie:</h4>
<!-- szukanie po ID -->
<form onSubmit="return getKsiegaByIdREST();" id="form">
	<p class="link">
		http://localhost:8080/projekt2/selection_ksiega/ksiega_id/{id}
	</p>
	<input type="text" name="ksiegaId" id="ksiegaId" placeholder="wpisz id..." required>
	<input type="submit" id="submit" value="getById">
</form>
<p class="info">
	Znajdź wpis z Czerwonej Księgi w bazie. Użyj widocznego formularza lub
	skopiuj jeden z powyższych linków zmieniając wartości
	w nawiasach klamrowych na faktyczne dane i użyj
	<a href="https://addons.mozilla.org/pl/firefox/addon/restclient/">
		Clienta RESTowego
	</a>
	.
</p>
<script src="static/javascript/simple.js"></script>
