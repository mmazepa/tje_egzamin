<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h4>Dodawanie zwierzęcia do bazy:</h4>
<p class="link">
	http://localhost:8080/projekt2/create/zwierze/{gatunek}/{imie}/{stopienZagrozenia}
</p>
<form onSubmit="return addZwierzeREST();" id="form">
	<input type="text" name="zwierzeGatunek" id="zwierzeGatunek" placeholder="wpisz gatunek..." required>/
	<input type="text" name="zwierzeImie" id="zwierzeImie" placeholder="wpisz imie..." required>/
	<select id="zwierzeCKGZ" name="zwierzeCKGZ" required>
		<option>EX</option>
		<option>EW</option>
		<option>CR</option>
		<option>EN</option>
		<option>VU</option>
		<option>NT</option>
		<option>LC</option>
		<option>-</option>
	</select>
	<input type="submit" id="submit" value="addZwierze">
</form>
<p class="info">
	Dodaj zwierzę do bazy. Użyj widocznego formularza lub
	skopiuj powyższy link zmieniając wartości
	w nawiasach klamrowych na faktyczne dane i użyj
	<a href="https://addons.mozilla.org/pl/firefox/addon/restclient/">
		Clienta RESTowego
	</a>
	.
</p>
<script src="static/javascript/simple.js"></script>
