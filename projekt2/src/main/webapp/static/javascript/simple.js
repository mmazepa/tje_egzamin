//FUNKCJE UŻYTKOWE:
//cel: zmiana wyświetlanego stanu strony, czytanie z pliku

function displayGoodMorning(){
	allText = "<h3>Witam na stronie!</h3>\
				<p>Użyj przycisków z lewej strony w celu wykonania akcji.</p>";
	document.getElementById("mainBox").innerHTML = allText;
}

function readTextFile(file){
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function (){
        if(rawFile.readyState === 4){
            if(rawFile.status === 200 || rawFile.status == 0){
                var allText = rawFile.responseText;
                document.getElementById("mainBox").innerHTML = allText;
            }
        }
    }
    rawFile.send(null);
}


//FUNKCJE NAWIGACYJNE:
//cel: przekierowanie do konkretnych ścieżek

function goSomewhere(url){
	location.href=url;
}

function hello(){
	goSomewhere("projekt2/hello/" + document.getElementById("message").value);
	return false;
}

//podpunkt 01: baza danych

function checkDB(){
	goSomewhere("projekt2/hsql/db");
	return false;
}

function resetDB(){
	goSomewhere("projekt2/hsql/reset");
	window.alert("Baza danych została zresetowana.");
	return false;
}

//podpunkt 02: tabela Zwierze

function selectAllZwierze(){
	goSomewhere("projekt2/selection_zwierze/zwierze_wszystkie");
	return false;
}

//podpunkt 03: tabela CzerwonaKsiega

function selectAllKsiega(){
	goSomewhere("projekt2/selection_ksiega/ksiega_wszystkie");
	return false;
}

//podpunkt 04: Client RESTowy

function httpRequest(theUrl, method, successStatus, failureStatus){
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() { 
		if(xmlHttp.readyState == 4 && xmlHttp.status == successStatus)
			alert(xmlHttp.responseText);
		if(xmlHttp.status == failureStatus)
			alert(xmlHttp.responseText);
	}
	xmlHttp.open(method, theUrl, true);
	xmlHttp.send(null);
}

//podpunkt 04a: tabela Zwierze

function getZwierzeByIdREST(){
	id = document.getElementById("zwierzeId").value;
	url = 'http://localhost:8080/projekt2/selection_zwierze/zwierze_id/' + id;
	httpRequest(url,"GET",200,404);
	return false;
}

function getZwierzeByGatunekREST(){
	gatunek = document.getElementById("zwierzeGatunek").value;
	url = 'http://localhost:8080/projekt2/selection_zwierze/zwierze_gatunek/' + gatunek;
	httpRequest(url,"GET",200,404);
	return false;
}

function getZwierzeByImieREST(){
	imie = document.getElementById("zwierzeImie").value;
	url = 'http://localhost:8080/projekt2/selection_zwierze/zwierze_imie/' + imie;
	httpRequest(url,"GET",200,404);
	return false;
}

function getZwierzeByCKGZREST(){
	ckgz = document.getElementById("zwierzeCKGZ").value;
	url = 'http://localhost:8080/projekt2/selection_zwierze/zwierze_ckgz/' + ckgz;
	httpRequest(url,"GET",200,404);
	return false;
}

function addZwierzeREST(){
	gatunek = document.getElementById("zwierzeGatunek").value;
	imie = document.getElementById("zwierzeImie").value;
	ckgz = document.getElementById("zwierzeCKGZ").value;
	url = 'http://localhost:8080/projekt2/create/zwierze/' + gatunek + '/' + imie + '/' + ckgz;
	httpRequest(url,"POST",201,404);
	return false;
}

function updateZwierzeREST(){
	id = document.getElementById("zwierzeId").value;
	imie = document.getElementById("zwierzeImie").value;
	url = 'http://localhost:8080/projekt2/update/zwierze/' + id + '/' + imie;
	httpRequest(url,"PUT",200,404);
	return false;
}

function deleteZwierzeREST(){
	id = document.getElementById("zwierzeId").value;
	url = 'http://localhost:8080/projekt2/delete/zwierze/' + id;
	httpRequest(url,"DELETE",200,404);
	return false;
}

//podpunkt 04b: tabela CzerwonaKsiega

function getKsiegaByIdREST(){
	id = document.getElementById("ksiegaId").value;
	url = 'http://localhost:8080/projekt2/selection_ksiega/ksiega_id/' + id;
	httpRequest(url,"GET",200,404);
	return false;
}
