<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h4>Usuwanie zwierzęcia z bazy:</h4>
<p class="link">
	http://localhost:8080/projekt2/delete/zwierze/{id}
</p>
<form onSubmit="return deleteZwierzeREST();" id="form">
	<input type="text" name="zwierzeId" id="zwierzeId" placeholder="wpisz id..." required>
	<input type="submit" id="submit" value="delZwierze">
</form>
<p class="info">
	Usuń zwierzę z bazy. Użyj widocznego formularza lub
	skopiuj powyższy link zmieniając wartości
	w nawiasach klamrowych na faktyczne dane i użyj
	<a href="https://addons.mozilla.org/pl/firefox/addon/restclient/">
		Clienta RESTowego
	</a>
	.
</p>
<script src="static/javascript/simple.js"></script>
