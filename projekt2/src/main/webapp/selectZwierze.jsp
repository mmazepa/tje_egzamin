<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h4>Wyszukiwanie zwierzęcia w bazie:</h4>
<!-- szukanie po ID -->
<form onSubmit="return getZwierzeByIdREST();" id="form">
	<p class="link">
		http://localhost:8080/projekt2/selection_zwierze/zwierze_id/{id}
	</p>
	<input type="text" name="zwierzeId" id="zwierzeId" placeholder="wpisz id..." required>
	<input type="submit" id="submit" value="getById">
</form>
<!-- szukanie po gatunku -->
<form onSubmit="return getZwierzeByGatunekREST();" id="form">
	<p class="link">
		http://localhost:8080/projekt2/selection_zwierze/zwierze_gatunek/{gatunek}
	</p>
	<input type="text" name="zwierzeGatunek" id="zwierzeGatunek" placeholder="wpisz gatunek..." required>
	<input type="submit" id="submit" value="getByGatunek">
</form>
<!-- szukanie po imieniu -->
<form onSubmit="return getZwierzeByImieREST();" id="form">
	<p class="link">
		http://localhost:8080/projekt2/selection_zwierze/zwierze_imie/{imie}
	</p>
	<input type="text" name="zwierzeImie" id="zwierzeImie" placeholder="wpisz imie..." required>
	<input type="submit" id="submit" value="getByName">
</form>
<!-- szukanie po wpisie z czerwonek księgi -->
<form onSubmit="return getZwierzeByCKGZREST();" id="form">
	<p class="link">
		http://localhost:8080/projekt2/selection_zwierze/zwierze_ckgz/{ckgz}
	</p>
	<select id="zwierzeCKGZ" name="zwierzeCKGZ" required>
		<option>EX</option>
		<option>EW</option>
		<option>CR</option>
		<option>EN</option>
		<option>VU</option>
		<option>NT</option>
		<option>LC</option>
		<option>-</option>
	</select>
	<input type="submit" id="submit" value="getByCKGZ">
</form>
<p class="info">
	Znajdź zwierzę w bazie. Użyj widocznego formularza lub
	skopiuj jeden z powyższych linków zmieniając wartości
	w nawiasach klamrowych na faktyczne dane i użyj
	<a href="https://addons.mozilla.org/pl/firefox/addon/restclient/">
		Clienta RESTowego
	</a>
	.
</p>
<script src="static/javascript/simple.js"></script>
