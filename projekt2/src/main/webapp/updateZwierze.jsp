<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h4>Zmiana zwierzęciu imienia w bazie:</h4>
<p class="link">
	http://localhost:8080/projekt2/update/zwierze/{id}/{noweImie}
</p>
<form onSubmit="return updateZwierzeREST();" id="form">
	<input type="text" name="zwierzeId" id="zwierzeId" placeholder="wpisz id..." required>/
	<input type="text" name="zwierzeImie" id="zwierzeImie" placeholder="wpisz imie..." required>/
	<input type="submit" id="submit" value="updateZwierze">
</form>
<p class="info">
	Zmień zwierzęciu imię w bazie. Użyj widocznego formularza lub
	skopiuj powyższy link zmieniając wartości
	w nawiasach klamrowych na faktyczne dane i użyj
	<a href="https://addons.mozilla.org/pl/firefox/addon/restclient/">
		Clienta RESTowego
	</a>
	.
</p>
<script src="static/javascript/simple.js"></script>
