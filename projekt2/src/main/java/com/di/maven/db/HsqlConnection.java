package myPackage;

import java.io.FileInputStream;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.BasicConfigurator;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Path("/hsql")
public class HsqlConnection {
	
	public static String answer = new String();
	public static Connection conn = null;
	public static Statement stmt = null;
	
	@GET
	@Path("/db")
	@Produces("text/html")
	public static Response showTables(){
		try {
			connectDB();
			answer = "<meta http-equiv='content-type' content='text/html;charset=utf-8' />";
			ResultSet rs = stmt.executeQuery("SELECT * FROM Zwierze");
			ResultSet rs2 = stmt.executeQuery("SELECT * FROM CzerwonaKsiega");
			wyswietlTabele(rs,"Lista Zwierząt",1);
			wyswietlTabele(rs2,"Wpisy z Czerwonej Księgi",2);
			stmt.close();
			disconnectDB();
		} catch (SQLException e){
			e.printStackTrace();
		}
		answer = answer.concat("<br/><a href='../../index.jsp'>powrót</a>");
		return Response.status(200).entity(answer).build();
	}
	
	@GET
	@Path("/reset")
	public void databaseActivity(){
		connectDB();
		String sqlFilesPath = "src/resources/sql_queries/";
		
		try {
			String dropIfExists = new String();
			dropIfExists = "DROP TABLE Zwierze IF EXISTS; DROP TABLE CzerwonaKsiega IF EXISTS;";
			stmt.executeQuery(dropIfExists);
			
			String createTable = new String();
			createTable = readFromFile(sqlFilesPath + "createTableZwierze.sql");
			createTable = createTable.concat(readFromFile(sqlFilesPath + "createTableCzerwonaKsiega.sql"));
			stmt.executeQuery(createTable);
			
			String insertInto = new String();
			insertInto = readFromFile(sqlFilesPath + "insertIntoZwierze.sql");
			insertInto = insertInto.concat(readFromFile(sqlFilesPath + "insertIntoCzerwonaKsiega.sql"));
			stmt.executeQuery(insertInto);
			
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace(); 
		} finally {
			disconnectDB();
		}
	}
	
	public static String readFromFile(String fileName){
		String textFromFile = new String();
		try(FileInputStream inputStream = new FileInputStream(fileName)) {     
			textFromFile = IOUtils.toString(inputStream);
		} catch(Exception e){
			e.printStackTrace();
			textFromFile = "<br/><b>Uwaga!</b> Pobranie tekstu z pliku " + fileName + " nie mogło zostać zrealizowane.<br/>";
		} finally {
			return textFromFile;
		}
	}
	
	public static void connectDB(){
		BasicConfigurator.configure();
		try {
			Class.forName("org.hsqldb.jdbcDriver");
		} catch (ClassNotFoundException e){
			System.out.println("UWAGA: Nie znaleziono klasy sterownika!\n"); 
		}
		try {
			conn = DriverManager.getConnection("jdbc:hsqldb:file:src/resources/hsql/hsql; shutdown=true", "root", ""); 
			stmt = conn.createStatement();
		} catch (SQLException e){
			System.out.println("UWAGA! Nie udało się nawiązać połączenia!\n");
		}
	}
	
	public static void disconnectDB(){
		if(conn != null) {
			try {
				conn.close(); 
			} catch (SQLException e) {
				e.printStackTrace(); 
			}
		}
		BasicConfigurator.resetConfiguration();
	}
	
	public static void wyswietlTabele(ResultSet rs, String title, Integer tableNumber){
		try {
			answer = answer.concat("<h4 style='background-color:silver'>" + title + ":</h4>");
			answer = answer.concat("<table style='font-size:smaller;'>");
			answer = answer.concat("<tr>");
			
			if(tableNumber == 1)
				answer = answer.concat("<th>ID</th><th>Gatunek</th><th>Imię</th><th>CKGZ</th>");
			if(tableNumber == 2)
				answer = answer.concat("<th>ID</th><th>Symbol</th><th>ZnaczeniePL</th><th>ZnaczenieANG</th>");
			answer = answer.concat("</tr>");
			
			while(rs.next()){
				answer = answer.concat("<tr>");
				String id = rs.getString(1);
				String value1 = rs.getString(2);
				String value2 = rs.getString(3);
				String value3 = rs.getString(4);
				answer = answer.concat("<td>" + id + "</td><td>" + value1 + "</td>");
				answer = answer.concat("<td>" + value2 + "</td><td>" + value3 + "</td>");
				answer = answer.concat("</tr>");
			}
			answer = answer.concat("</table>");
			rs.close();
		} catch(SQLException e) {
			System.out.println("UWAGA! Wyświetlenie tabeli \"" + title + "\" nie mogło zostać zrealizowane!");
		}
	}
}
