package myPackage;

import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Entity;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "Zwierze")
@NamedQueries({
	@NamedQuery(name = "Zwierze.all", query = "Select z from Zwierze z"),
	@NamedQuery(name = "Zwierze.findId", query = "Select z from Zwierze z WHERE z.id = :id"),
	@NamedQuery(name = "Zwierze.findGatunek", query = "Select z from Zwierze z WHERE z.gatunek = :gatunek"),
	@NamedQuery(name = "Zwierze.findImie", query = "Select z from Zwierze z WHERE z.imie = :imie"),
	@NamedQuery(name = "Zwierze.findCKGZ", query = "Select z from Zwierze z WHERE z.ckgz = :ckgz")
})
@XmlRootElement
public class Zwierze {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String gatunek = new String();
	private String imie = new String();
	private String ckgz = new String();
	private Integer ck_id;
		
	public Zwierze(){
		super();
	}
	
	public Zwierze(int id, String gatunek, String imie, String ckgz, Integer ck_id){
		super();
		this.id = id;
		this.gatunek = gatunek;
		this.imie = imie;
		this.ckgz = ckgz;
		this.ck_id = ck_id;
	}
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public String getGatunek(){
		return gatunek;
	}
	
	public void setGatunek(String gatunek){
		this.gatunek = gatunek;
	}
	
	public String getImie(){
		return imie;
	}
	
	public void setImie(String imie){
		this.imie = imie;
	}
	
	public String getCKGZ(){
		return ckgz;
	}
	
	public void setCKGZ(String ckgz){
		this.ckgz = ckgz;
	}
	
	@ManyToOne
	@JoinColumn(name = "id", nullable = false)
	public Integer getCzerwonaKsiega(){
		return ck_id;
	}
	
	public void setCzerwonaKsiega(Integer ck_id){
		this.ck_id = ck_id;
	}
	
	@Override
	public String toString() {
		return "Zwierze [id=" + id + ", gatunek=" + gatunek + ", imie=" + imie + ", ckgz=" + ckgz + ", ck_id=" + ck_id + "]";
	}
}
