package myPackage;

import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Entity;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Table(name = "CzerwonaKsiega")
@NamedQueries({
	@NamedQuery(name = "CzerwonaKsiega.all", query = "Select ck from CzerwonaKsiega ck"),
	@NamedQuery(name = "CzerwonaKsiega.findId", query = "Select ck from CzerwonaKsiega ck WHERE ck.id = :id"),
})
@XmlRootElement
public class CzerwonaKsiega {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String symbol = new String();
	private String znaczeniePl = new String();
	private String znaczenieAng = new String();
	
	public CzerwonaKsiega(){
		super();
	}
	
	public CzerwonaKsiega(int id, String symbol, String znaczeniePl, String znaczenieAng){
		super();
		this.id = id;
		this.symbol = symbol;
		this.znaczeniePl = znaczeniePl;
		this.znaczenieAng = znaczenieAng;
	}
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public String getSymbol(){
		return symbol;
	}
	
	public void setSymbol(String symbol){
		this.symbol = symbol;
	}
	
	public String getZnaczeniePl(){
		return znaczeniePl;
	}
	
	public void setZnaczeniePl(String znaczeniePl){
		this.znaczeniePl = znaczeniePl;
	}
	
	public String getZnaczenieAng(){
		return znaczenieAng;
	}
	
	public void setZnaczenieAng(String znaczenieAng){
		this.znaczenieAng = znaczenieAng;
	}
	
	@Override
	public String toString() {
		return "CzerwonaKsiega [id=" + id + ", symbol=" + symbol + ", znaczeniePl=" + znaczeniePl + ", znaczenieAng=" + znaczenieAng + "]";
	}
}
