package myPackage;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.sql.ResultSet;
import java.sql.SQLException;

@Path("/create")
public class CreateZwierze {
	
	@EJB
	static ZwierzeManager zm = new ZwierzeManager();
	
	@POST
	@Path("/zwierze/{gatunek}/{imie}/{ckgz}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static Response addZwierze(
		@PathParam("gatunek") String gatunek,
		@PathParam("imie") String imie,
		@PathParam("ckgz") String ckgz
	){
		
		Zwierze z = new Zwierze();
		z.setGatunek(gatunek);
		z.setImie(imie);
		z.setCKGZ(ckgz);
		
		zm.insertZwierze(z);
		
		String output = zm.makeJsonPretty(z);
		return Response.status(201).entity(output).build();
	}
}
