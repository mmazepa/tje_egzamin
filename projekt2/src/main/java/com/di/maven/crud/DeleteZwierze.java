package myPackage;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.sql.ResultSet;
import java.sql.SQLException;

@Path("/delete")
public class DeleteZwierze {
	
	@EJB
	static ZwierzeManager zm = new ZwierzeManager();
	
	@DELETE
	@Path("/zwierze/{id}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static Response deleteZwierze(@PathParam("id") Integer id){
		
		int deleted = 0;
		deleted = zm.deleteZwierze(id);
		
		if(deleted > 0){
			return Response.status(200).entity("Usunięto zwierze o ID: " + id).build();
		} else if(deleted == 0){
			return Response.status(404).entity("ID = " + id + " nie znajduje się w bazie danych.").build();
		}
		return Response.status(200).build();
	}
}
