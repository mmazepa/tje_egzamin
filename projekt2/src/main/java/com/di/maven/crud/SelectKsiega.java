package myPackage;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Persistence;
import javax.persistence.Query;

import java.sql.ResultSet;
import java.sql.SQLException;

@Path("/selection_ksiega")
public class SelectKsiega {
	
	@EJB
	static CzerwonaKsiegaManager ckm = new CzerwonaKsiegaManager();
	
	@GET
	@Path("/ksiega_id/{id}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static Response selectKsiegaById(@PathParam("id") Integer id){
		CzerwonaKsiega ck = new CzerwonaKsiega();
		ck = ckm.getKsiegaById(id);
		if(ck.getId() == null){
			String returnInfo = "Nie znaleziono wpisu o podanym ID (" + id + ").";
			return Response.status(200).entity(returnInfo).build();
		}
		String output = ckm.makeJsonPretty(ck);
		return Response.status(200).entity(output).build();
	}
	
	@GET
	@Path("/ksiega_wszystkie")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static Response selectAllKsiega(){
		CzerwonaKsiega ck = new CzerwonaKsiega();
		Map<Integer, CzerwonaKsiega> ksiegMap = new HashMap<Integer, CzerwonaKsiega>();
		ksiegMap = ckm.selectAllKsiega();
		String output = ckm.makeJsonPretty(ksiegMap);
		return Response.status(200).entity(output).build();
	}
}
