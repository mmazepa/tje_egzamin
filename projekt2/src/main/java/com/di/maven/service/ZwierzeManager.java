package myPackage;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
public class ZwierzeManager {
	
	public Zwierze getZwierzeById(Integer id){
		Zwierze z = new Zwierze();
		try {	
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze WHERE id=" + id + ";");
			while(rs.next()){
				Integer id_new = Integer.parseInt(rs.getString(1));
				String gatunek = rs.getString(2);
				String imie = rs.getString(3);
				String ckgz = rs.getString(4);
				z.setId(id_new);
				z.setGatunek(gatunek);
				z.setImie(imie);
				z.setCKGZ(ckgz);
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		if(z.getId() == null) return new Zwierze();
		return z;
	}
	
	public Zwierze getZwierzeByGatunek(String gatunek){
		Zwierze z = new Zwierze();
		try {	
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze WHERE gatunek='" + gatunek + "';");
			while(rs.next()){
				Integer id = Integer.parseInt(rs.getString(1));
				String gatunek_new = rs.getString(2);
				String imie = rs.getString(3);
				String ckgz = rs.getString(4);
				z.setId(id);
				z.setGatunek(gatunek_new);
				z.setImie(imie);
				z.setCKGZ(ckgz);
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		if(z.getId() == null) return new Zwierze();
		return z;
	}
	
	public Zwierze getZwierzeByImie(String imie){
		Zwierze z = new Zwierze();
		try {	
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze WHERE imie='" + imie + "';");
			while(rs.next()){
				Integer id = Integer.parseInt(rs.getString(1));
				String gatunek = rs.getString(2);
				String imie_new = rs.getString(3);
				String ckgz = rs.getString(4);
				z.setId(id);
				z.setGatunek(gatunek);
				z.setImie(imie_new);
				z.setCKGZ(ckgz);
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		if(z.getId() == null) return new Zwierze();
		return z;
	}
	
	public Zwierze getZwierzeByCKGZ(String ckgz){
		Zwierze z = new Zwierze();
		try {	
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze WHERE ckgz='" + ckgz + "';");
			while(rs.next()){
				Integer id = Integer.parseInt(rs.getString(1));
				String gatunek = rs.getString(2);
				String imie = rs.getString(3);
				String ckgz_new = rs.getString(4);
				z.setId(id);
				z.setGatunek(gatunek);
				z.setImie(imie);
				z.setCKGZ(ckgz);
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		if(z.getId() == null) return new Zwierze();
		return z;
	}
	
	public void insertZwierze(Zwierze z){
		try {
			HsqlConnection.connectDB();
			String sql = "INSERT INTO Zwierze (gatunek,imie,ckgz) VALUES('" + z.getGatunek() + "','" + z.getImie() + "','" + z.getCKGZ() + "');";
			HsqlConnection.stmt.executeQuery(sql);
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public int updateZwierze(int id, String noweImie){
		Zwierze z = new Zwierze();
		int updated = 0;
		try {
			z = getZwierzeById(id);
			HsqlConnection.connectDB();
			String sql = "UPDATE Zwierze SET imie='" + noweImie + "' WHERE id=" + id + ";";
			updated = HsqlConnection.stmt.executeUpdate(sql);
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		return updated;
	}
	
	public int deleteZwierze(int id){
		int deleted = 0;
		try {
			HsqlConnection.connectDB();
			String sql = "DELETE FROM Zwierze WHERE id=" + id + ";";
			deleted = HsqlConnection.stmt.executeUpdate(sql);
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		return deleted;
	}
	
	public Map<Integer, Zwierze> selectAllZwierze(){
		Zwierze z = new Zwierze();
		Map<Integer, Zwierze> zwierzMap = new HashMap<Integer, Zwierze>();
		try {
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM Zwierze;");
			while(rs.next()){
				Integer id = Integer.parseInt(rs.getString(1));
				String gatunek = rs.getString(2);
				String imie = rs.getString(3);
				String ckgz = rs.getString(4);
				z.setId(id);
				z.setGatunek(gatunek);
				z.setImie(imie);
				z.setCKGZ(ckgz);
				zwierzMap.put(id,new Zwierze(id,gatunek,imie,ckgz,null));
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		return zwierzMap;
	}
	
	//DODATKOWE METODY:
	//cel: ładniejsze wyświetlanie JSONów
	
	public static String makeJsonPretty(Map<Integer, Zwierze> myMap){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(myMap);
		return jsonOutput;
	}
	
	public static String makeJsonPretty(Zwierze myZwierze){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(myZwierze);
		return jsonOutput;
	}
}
