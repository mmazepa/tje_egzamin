package myPackage;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
public class CzerwonaKsiegaManager {
	
	public CzerwonaKsiega getKsiegaById(Integer id){
		CzerwonaKsiega ck = new CzerwonaKsiega();
		try {	
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM CzerwonaKsiega WHERE id=" + id + ";");
			while(rs.next()){
				Integer id_new = Integer.parseInt(rs.getString(1));
				String symbol = rs.getString(2);
				String znaczeniePl = rs.getString(3);
				String znaczenieAng = rs.getString(4);
				ck.setId(id_new);
				ck.setSymbol(symbol);
				ck.setZnaczeniePl(znaczeniePl);
				ck.setZnaczenieAng(znaczenieAng);
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		if(ck.getId() == null) return new CzerwonaKsiega();
		return ck;
	}
	
	public Map<Integer, CzerwonaKsiega> selectAllKsiega(){
		CzerwonaKsiega ck = new CzerwonaKsiega();
		Map<Integer, CzerwonaKsiega> ksiegMap = new HashMap<Integer, CzerwonaKsiega>();
		try {
			HsqlConnection.connectDB();
			ResultSet rs = HsqlConnection.stmt.executeQuery("SELECT * FROM CzerwonaKsiega;");
			while(rs.next()){
				Integer id = Integer.parseInt(rs.getString(1));
				String symbol = rs.getString(2);
				String znaczeniePl = rs.getString(3);
				String znaczenieAng = rs.getString(4);
				ck.setId(id);
				ck.setSymbol(symbol);
				ck.setZnaczeniePl(znaczeniePl);
				ck.setZnaczenieAng(znaczenieAng);
				ksiegMap.put(id,new CzerwonaKsiega(id,symbol,znaczeniePl,znaczenieAng));
			}
			rs.close();
			HsqlConnection.disconnectDB();
		} catch(SQLException e){
			e.printStackTrace();
		}
		return ksiegMap;
	}
	
	//DODATKOWE METODY:
	//cel: ładniejsze wyświetlanie JSONów
	
	public static String makeJsonPretty(Map<Integer, CzerwonaKsiega> myMap){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(myMap);
		return jsonOutput;
	}
	
	public static String makeJsonPretty(CzerwonaKsiega myKsiega){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonOutput = gson.toJson(myKsiega);
		return jsonOutput;
	}
}
